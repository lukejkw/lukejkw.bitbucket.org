$(function(){

    var $searchInput = $('#txtSearch');
    var $searchList = $('#search-list');
    $searchInput.focus(function(){
                    $searchList.show();
                })
                .blur(function(){
                    $searchList.hide();
                });

    // Functions
    function init() {
        // All Init code
        //new ShareButton();

        var options = {
            valueNames: [ 'tech', 'url' ]
        };

        var list = new List('search-container', options);
    }
   
    init();
});