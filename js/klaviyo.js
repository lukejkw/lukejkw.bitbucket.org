var _learnq = _learnq || [];

_learnq.push(['account', 'ifFrLJ']);

(function () {
    var b = document.createElement('script'); b.type = 'text/javascript'; b.async = true;
    b.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'a.klaviyo.com/media/js/analytics/analytics.js';
    var a = document.getElementsByTagName('script')[0]; a.parentNode.insertBefore(b, a);
})();
KlaviyoSubscribe.attachFlyoutSignUp({
    list: 'iNntm8',
    delay_scroll: 0,
    delay_seconds: 1,
    content: {
        clazz: ' klaviyo_flyout_iNntm8',
        header: "Like what you see?",
        subheader: "Keep in touch but adding your email to my newsletter. I dont spam! ",
        button: "Subscribe",
        success: "Thanks! Check your email for a confirmation.",
        styles: '.klaviyo_flyout.klaviyo_flyout_iNntm8 {  font-family: "Helvetica Neue", Arial}.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_topbar {  background-color: #0064cd;}.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_header {  color:#222;}.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_subheader {  color:#222;}.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_submit_button,.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_submit_button span {  background-color:#0064cd;  background-image: none;   border-radius: 2px;}.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_submit_button:hover,.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_submit_button span:hover {  background-color:#0064cd;  background-image: none; }.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_fieldset .klaviyo_field_group input[type=text],.klaviyo_flyout.klaviyo_flyout_iNntm8 .klaviyo_fieldset .klaviyo_field_group input[type=email] {  border-radius: 2px;}'
    },
    modal_content: false
});